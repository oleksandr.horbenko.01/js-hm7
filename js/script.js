// 1. DOM - обєктна модель документа.Кожен HTML тег є обєктом і належить до дом-дерева.Дом визначає атрибути, класи та методи
// 2.Властивість innerHTML отримує вміст HTML елемента, а innerText повертає тільки текст, без пробілів і тегів
// 3.Звернутися можна за допомогою: 
//    getElementById (value)
//    getElementsByTagName (value)
//    getElementsByClassName (value)
//    querySelector (value)
//    querySelectorAll (value)
//    Який спосіб краще? Все в залежності від того що нам треба, і що ми хочемо отримати.


// 1 wse
const searchAllP = document.querySelectorAll('p');

searchAllP.forEach((elem) => {
    elem.style.backgroundColor = "#ff0000";
});

// 2 wse
const searchPoId = document.getElementById('optionsList');
console.log(searchPoId);

const search = searchPoId.parentElement;
console.log(search);

const searchChild = searchPoId.childNodes;
console.log(searchChild);

// 3 wse
const searchP = document.getElementById('testParagraph');
searchP.innerText = "This is a paragraph";

// 4,5 wse
const searchClassMain = document.querySelector('.main-header');
const result = [...searchClassMain.children];
console.log(result);

result.forEach((elem) => {
    elem.className = 'nav-item';
});

console.log(result);

// 6
const section = document.querySelectorAll('.section-title')
console.log(section);

section.forEach((element) => {
    element.classList.remove('section-title');
});

console.log(section);
    